import java.util.ArrayList;

public class MovingAverageImpl implements MovingAverage<Integer> {

    private int n;
    private int numOfElements;
    private double lastNSum;
    private double movingAverage;
    private ArrayList<Integer> elements;

    public MovingAverageImpl(int n) {
        this.n = n;
        this.numOfElements = 0;
        this.lastNSum = 0;
        this.movingAverage = 0.0;
        this.elements = new ArrayList<>();
    }

    @Override
    public double getMovingAverage() {
        return movingAverage;
    }

    @Override
    public void addElement(Integer integer) {
        elements.add(integer);
        lastNSum += integer;
        numOfElements++;

        if (numOfElements > n) {
            lastNSum -= elements.get(numOfElements - n - 1);
            movingAverage = lastNSum / n;
        } else {
            movingAverage = lastNSum / numOfElements;
        }
    }

    @Override
    public Integer getElement(int index) {
        if (index >= numOfElements) {
            throw new IndexOutOfBoundsException(
                    String.format(
                            "Index %s is out of bounds. There are only %s elements in total.", index, numOfElements));
        }
        return elements.get(index);
    }

    @Override
    public void setNewN(int n) {
        if (this.n != n && (numOfElements > this.n || numOfElements > n)) {
            lastNSum = 0;
            for (int i = numOfElements > n ? numOfElements - n : 0; i < numOfElements; i++) {
                lastNSum += elements.get(i);
            }
            movingAverage = numOfElements > n ? lastNSum / n : lastNSum / numOfElements;
        }
        this.n = n;
    }
}
