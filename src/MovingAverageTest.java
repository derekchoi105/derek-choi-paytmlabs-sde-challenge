import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class MovingAverageTest {

    private MovingAverage<Integer> movingAverage;

    @Before
    public void init() {
        movingAverage = new MovingAverageImpl(6);
    }

    @Test
    public void testAddElement() {
        movingAverage.addElement(99);
        movingAverage.addElement(32);
        movingAverage.addElement(3);
        movingAverage.addElement(1);
        movingAverage.addElement(4);
        movingAverage.addElement(5);
        Assert.assertEquals(movingAverage.getElement(1), 32,0.0);
        Assert.assertEquals(movingAverage.getMovingAverage(), 24.0,0.0);
        movingAverage.addElement(22);
        Assert.assertEquals(movingAverage.getElement(6), 22,0.0);
        Assert.assertEquals(movingAverage.getMovingAverage(), 11.1666,0.0001);
    }

    @Test
    public void testGetElement() {
        movingAverage.addElement(12);
        movingAverage.addElement(88);
        Assert.assertEquals(movingAverage.getElement(1), 88,0.0);
        movingAverage.addElement(22);
        movingAverage.addElement(50);
        Assert.assertEquals(movingAverage.getElement(3), 50,0.0);
    }

    @Test
    public void testGetElementOutOfBounds() {
        movingAverage.addElement(77);
        movingAverage.addElement(2);
        movingAverage.addElement(53);
        movingAverage.addElement(21);
        movingAverage.addElement(8);
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
            movingAverage.getElement(8);
        });
    }

    @Test
    public void testGetMovingAverage() {
        movingAverage.addElement(20);
        movingAverage.addElement(12);
        movingAverage.addElement(22);
        Assert.assertEquals(movingAverage.getMovingAverage(), 18.0, 0.0);
        movingAverage.addElement(1);
        movingAverage.addElement(15);
        movingAverage.addElement(30);
        Assert.assertEquals(movingAverage.getMovingAverage(), 16.6666, 0.0001);
        movingAverage.addElement(67);
        movingAverage.addElement(12);
        Assert.assertEquals(movingAverage.getMovingAverage(), 24.5, 0.0);
    }

    @Test
    public void testSetNewN() {
        movingAverage.addElement(1);
        movingAverage.addElement(2);
        movingAverage.addElement(3);
        movingAverage.addElement(4);
        movingAverage.addElement(5);
        movingAverage.addElement(6);
        Assert.assertEquals(movingAverage.getMovingAverage(), 3.5, 0.0);
        movingAverage.setNewN(3);
        Assert.assertEquals(movingAverage.getMovingAverage(), 5, 0.0);
        movingAverage.setNewN(10);
        Assert.assertEquals(movingAverage.getMovingAverage(), 3.5, 0.0);
        movingAverage.setNewN(8);
        Assert.assertEquals(movingAverage.getMovingAverage(), 3.5, 0.0);
        movingAverage.setNewN(8);
        Assert.assertEquals(movingAverage.getMovingAverage(), 3.5, 0.0);
        movingAverage.setNewN(4);
        Assert.assertEquals(movingAverage.getMovingAverage(), 4.5, 0.0);
    }
}
