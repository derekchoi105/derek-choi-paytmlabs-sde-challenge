public interface MovingAverage<T> {
    /**
     * Get the moving average for the last n elements of the data structure. If n is greater than the total number of
     * elements, return the moving average for all the elements.
     * @return the average of the last n elements.
     */
    double getMovingAverage();

    /**
     * Add an element to the data structure. Recalculate the moving average based on the added element.
     * @param element element to be added
     */
    void addElement(T element);

    /**
     * Get the element from the data structure for a given index.
     * @param index index of the element to be retrieved
     * @return the element at the index provided
     */
    T getElement(int index);

    /**
     * Set a new n for the data structure and recalculate the moving average, if required, based on the new n.
     * Recalculating the moving average is not required if n remains the same or if both the previous n and the new n
     * are greater than the number of elements in the data structure.
     * @param n the number of elements we want averaged in our moving average
     */
    void setNewN(int n);
}
