## Data Structure for the Moving Average of the last `n` elements

This data structure provides the moving average of the last `n` elements added. You can add elements as well as get elements based on index.

This can be simply implemented by holding all the elements in a list and every time you want the moving average you'd loop through the last `n` elements and calculate the average. But this is not efficient. 

We can implement this data structure in a more efficient way by keeping two instance variables handy that would overcome the need to use O(n) operations.

- `lastNSum`: This variable will keep track of the sum of the last `n` elements. Every time we add an element to our data structure, we will add it to `lastNSum`. But if the number of elements in the data structure is greater than `n`, then we will subtract `lastNSum` by the element that is no longer in our `n` sized window, which is the number directly to the left of the window. By keeping track of the sum of the last `n` elements in this way, we overcome the need to have to always loop through the last `n` elements to find the moving average, which would run in O(n) time. Finally, we can simply divide `lastNSum` with n (OR by the total number of elements if `n` is greater than the total number of elements) to always have the moving average. This variable helps keep our `getMovingAverage()` operation running at O(1) time.

- `numOfElements`: This variable helps us keep track of the total number of elements in our data structure. It alleviates the need to calculate the number of elements with operations like `ArrayList.size()`, which runs in O(n) time. By keeping track of the total number of elements this way, we keep both our `addElement()` operation and calculation of the moving average running at O(1) time.

Thus, our runtime complexity for the following operations are as follows:

- `addElement(T element)` - Adding an element to the data structure: O(1)
- `getElement(int index)` - Getting an element from the data structure: O(1)
- `getMovingAverage()` - Getting the moving average for the last `n` elements added: O(1) 
